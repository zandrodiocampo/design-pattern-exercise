package design_pattern_exercise;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        BookCollectionDatabase bookDB = BookCollectionDatabase.getInstance();

        bookDB.addBook(1, "To Kill a Mockingbird");
        bookDB.addBook(2, "The Great Gatsby");
        bookDB.addBook(3, "The Catcher In The Eye");
        bookDB.addBook(4, "Don Quixote");
        bookDB.addBook(5,"Pride and Prejudice");

        bookDB.viewBooks();

        String removedBookName = bookDB.removeBook(2);
        System.out.println("Removed book: " + removedBookName);

        bookDB.viewBooks();
    }
}

