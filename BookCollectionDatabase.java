package design_pattern_exercise;

import java.util.HashMap;
import java.util.Map;

public class BookCollectionDatabase {
    private static BookCollectionDatabase instance;
    private Map<Integer, String> bookMap;

    private BookCollectionDatabase() {
        bookMap = new HashMap<>();
    }

    public static synchronized BookCollectionDatabase getInstance() {
        if (instance == null) {
            instance = new BookCollectionDatabase();
        }
        return instance;
    }

    public void addBook(int id, String bookName) {
        bookMap.put(id, bookName);
    }

    public String removeBook(int id) {
        String bookRemoved = bookMap.remove(id);
        return bookRemoved;
    }

    public void viewBooks() {
        System.out.println("Available Books: ");
        for (Map.Entry<Integer, String> entry : bookMap.entrySet()) {
            System.out.println("ID: " + entry.getKey() + "  Name: " + entry.getValue());
        }
    }
}
